
export const CANVAS_BG = '#dde5e8';

export const BG_COLOR = '#fff';

export const BORDER_RADIUS = '4px';

export const DARK_GRAY = '#2b333b';

export const LIGHT_GRAY = '#aaa';

export const HIGH_BLUE = '#247fff';

export const OPTIONS_FONT = 'Roboto, sans-serif';

export const MEDIUM_SIZE_FONT = '14px';

export const DIM_BLACK = '#333333';