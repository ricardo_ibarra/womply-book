import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react';
// import '@testing-library/jest-dom/extend-expect';

import { MenuSeparator, DefaultMenu, AlternateMenu } from './DesktopMenu';
import { menuResource, AlternateMenuResource } from '../../resources/menu-resource';

// for this type of component snapshot should be enough
test('Test snapshot for MenuSeparator', () => {
    const component = renderer.create(
        <MenuSeparator></MenuSeparator>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test('Test snapshot for Default Menu', () => {
    const component = renderer.create(
        <DefaultMenu menuData={menuResource.en}></DefaultMenu>
    )

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
})

test('Menu Renders clickable elements', () => {
    const { getByText } = render(<DefaultMenu menuData={menuResource.en}></DefaultMenu>);
    expect(getByText('Home')).toBeInTheDocument();
    expect(getByText('Business Overview')).not.toBeVisible();
    const parentLink = getByText('Monitor your Business');
    expect(parentLink).toBeInTheDocument();
    fireEvent.click(parentLink);
    expect(getByText('Business Overview')).toBeVisible();
    fireEvent.click(parentLink);
    const alternativeOption = getByText('Boost your Reputation');
    fireEvent.click(alternativeOption);
    expect(getByText('Business Overview')).not.toBeVisible();
    expect(getByText('Manage Review')).toBeVisible();
})

test('Test menu options click', () => {
    const groupClick = (option) => { 
        expect(option).toBeDefined();
        expect(option.id).toBeGreaterThan(0);
        expect(option.label).toBeDefined();
     }; 
    const subClick = (option) => { 
        expect(option).toBeDefined();
        expect(option.id).toBeGreaterThan(0);
        expect(option.parentId).toBeDefined();
    };
    
    const { getByText } = render(
                                <DefaultMenu 
                                    menuData={menuResource.en}
                                    groupClick={groupClick}
                                    subClick={subClick}
                                    >
                                </DefaultMenu>);

    const firstLink = getByText('Monitor your Business')
    expect(firstLink).toBeVisible();
    fireEvent.click(firstLink);
    const subLink = getByText('Business Overview');
    expect(subLink).toBeVisible();
    fireEvent.click(subLink);
});

test('Test snapshot for Alternate Menu', () => {
    const component = renderer.create(
        <AlternateMenu menuData={menuResource.en}></AlternateMenu>
    )

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
})

test('Alternate menu does not crash if there is no data', () => {
    const { container } = render(<AlternateMenu></AlternateMenu>)
    expect(container.querySelector('ul')).toBeDefined();
});


// TODO: Pending work
// Make npm package and make sure you can import it.
// make the link an element you could send down the menu for sub options (to avoid programtic routing)





