import React from 'react';


import { Canvas } from '../components/Workspace';

import { SummaryPie, SummaryPercentage } from '../components/SummaryStats'

export default {
    title: 'Summary',
    component: SummaryPie
}

export const SummaryPieKPI = () => (
    <Canvas>
        <div style={{ width: 400 }}>
            <SummaryPie 
                title="Total Customers" 
                total="14,891"
                titleWidth="100px"
                percentage="50%"
                >
            </SummaryPie>
        </div>
        
    </Canvas>
)

export const SummaryPercentageKPI = () => (
    <Canvas>
        <div style={{ width: 400 }}>
            <SummaryPercentage 
                title="Customers found by Womply" 
                total="14,891"
                titleWidth="120px"
                percentage="97%"
                >
            </SummaryPercentage>
        </div>
        
    </Canvas>
)

