import { create } from '@storybook/theming/create';

export default create({
  base: 'light',

  brandTitle: 'Womply StoryBook',
  brandUrl: 'https://womply.com',
  brandImage: 'https://res.cloudinary.com/www-codervelop-com/image/upload/v1583279404/womply-storybook_q9e3vs.png',
});