import React from 'react';
import styled from 'styled-components';

import { BG_COLOR, DIM_BLACK, BORDER_RADIUS } from '../themes/styles';

export const SummaryPanel = styled.div`
    background-color: ${BG_COLOR};
    padding: 4px 20px;
    font-family: Roboto;
    border-radius: ${BORDER_RADIUS};
    height: 60px;
`;

export const FlexRow = styled.div`
    display: flex;
    direction: columns;
`;

export const FlexCell = styled.div`
    flex-grow: 1;
    margin-top: 15px;
`;

export const ChartCell = styled.div`
    margin-top: 10px;
`;

export const SummaryTitle = styled.h5`
    font-weight: bold;
    color: ${DIM_BLACK};
    font-size: 12px;
    text-transform: uppercase;
    margin: 0;
`;

export const SummaryTotal = styled.h4`
    font-size: 30px;
    padding:0;
    margin: 0;
`;


export const SummaryPieGraph = styled.div`
    width: 40px; 
    height: 40px;
    border-radius: 50%;
    background: #2a8aff;
    background-image:
    linear-gradient(to right, transparent 50%, #ffd700 0);
`;

export const SummaryPercentageGraphStyle = styled.div`
    width: 36px; 
    height: 36px;
    border-radius: 50%;
    border: solid 4px #2a8aff;
    text-align:center;
    span {
        display:inline-block;
        margin-top: 10px;
        font-size: 14px;
    }
`;

export const SummaryPercentageChart = ({ percentage }) => (
    <SummaryPercentageGraphStyle><span>{ percentage }</span></SummaryPercentageGraphStyle>
)



export const SummaryStat = ({ title, ChartType, total = 0, titleWidth = 'auto', percentage }) => {
    return (
        <SummaryPanel>
            <FlexRow>
                <FlexCell><SummaryTitle style={{ width:titleWidth  }}>{ title }</SummaryTitle></FlexCell>
                <FlexCell><SummaryTotal>{ total }</SummaryTotal></FlexCell>
                <ChartCell><ChartType percentage={percentage}></ChartType></ChartCell>
            </FlexRow>
        </SummaryPanel>
    );
}
export const SummaryPie = (props) => (
    <SummaryStat ChartType={SummaryPieGraph} {...props} ></SummaryStat>
);

export const SummaryPercentage = (props) => (
    <SummaryStat ChartType={SummaryPercentageChart} {...props} ></SummaryStat>
);