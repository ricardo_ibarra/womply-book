import React from 'react';

import { Logo } from './index';

import renderer from 'react-test-renderer';

import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({adapter: new Adapter()});

test('Logo DOM elements are OK', () => {
    const component = renderer.create(
        <Logo ></Logo>
    )

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
})

test('Logo shallow render', () => {
    const component = shallow(<Logo alt="WomplyBook"></Logo>);
    const img = component.find('img');
    const alt = img.prop('alt');
    expect(alt).toEqual('WomplyBook');
});


//TODO: 
// Add test coverage 
// write tests for all existing components
// do the gatsby POC

