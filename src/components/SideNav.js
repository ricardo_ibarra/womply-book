import React from 'react';
import styled from 'styled-components';
import {  DARK_GRAY, OPTIONS_FONT, MEDIUM_SIZE_FONT } from '../themes/styles';

export const SideNav = styled.div`
    width: 100%;
    min-height: 100vh;
    background-color: ${DARK_GRAY};
    font-family: ${OPTIONS_FONT};
    outline: none;
    font-size: ${MEDIUM_SIZE_FONT};
`;

