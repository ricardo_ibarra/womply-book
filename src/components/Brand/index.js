import React from 'react';
import styled from 'styled-components';

export const LogoWrapper = styled.div`
    display: block;
    margin-bottom: 12px;
    padding: 25px;
`;


export const Logo = ({ alt }) => (
    <LogoWrapper>
        <img src="https://app00.testing.womply.com/dashboard/assets/images/womply.svg" alt={alt}/>
    </LogoWrapper>
)