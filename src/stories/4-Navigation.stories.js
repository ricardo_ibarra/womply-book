import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
// data
import { menuResource, AlternateMenuResource } from '../resources/menu-resource';
//UI elements
import { SideNav } from '../components/SideNav';
import { Logo } from '../components/Brand';
import {  MenuSeparator, DefaultMenu, AlternateMenu } from '../components/SideNavigation/DesktopMenu';


export default {
    title: 'Navigation',
    component: Button,
  };
  


export const SideNavigationEN= () => {
 const _menuData = menuResource.en.slice();
 const _alternateMenuData = AlternateMenuResource.en.slice();

 return (
     <SideNav>
         <Logo/>
        <DefaultMenu 
            menuData={_menuData} 
            groupClick={action('Additional action based on parent logic')} 
            subClick={action('Routing logic at parent object')}
        ></DefaultMenu>
        <MenuSeparator/>
        <AlternateMenu 
            menuData={_alternateMenuData} 
            groupClick={action('Additional action based on parent logic')} 
            subClick={action('Routing logic at parent object')}
        ></AlternateMenu>
    </SideNav>
    )
 }

 export const SideNavigationES= () => {
    const _menuData = menuResource.es.slice();
    const _alternateMenuData = AlternateMenuResource.es.slice();
    return (
        <SideNav>
            <Logo/>
            <DefaultMenu 
                menuData={_menuData} 
                groupClick={action('Additional action based on parent logic')} 
                subClick={action('Routing logic at parent object')}
            ></DefaultMenu>
            <MenuSeparator/>
            <AlternateMenu 
                menuData={_alternateMenuData} 
                groupClick={action('Additional action based on parent logic')} 
                subClick={action('Routing logic at parent object')}
            ></AlternateMenu>
        </SideNav>
       )
    }
